import sys
if len(sys.argv) < 2:
    base_path = ".."
else:
    base_path = sys.argv[1]
f = open(base_path + "/linio/linio/settings_example.py", "r")
res_f = open(base_path + "/linio/linio/settings.py", "w+")
for line in f:
    if line == 'ALLOWED_HOSTS = []\n':
        line = line[:-2] + "'*']\n"
    if line == 'DEBUG = True\n':
        line = "DEBUG = False\n"
    res_f.write(line)
res_f.close()
