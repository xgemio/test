#!/bin/bash

docker ps | grep linio | tr -s ' '| cut -f11 -d' ' |
{
    while read line;
    do docker kill $line;
    done
}