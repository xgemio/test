  FROM ubuntu:14.04
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install -y python3 python3-pip nginx supervisor
RUN pip3 install django djangorestframework gunicorn

# Setup application
RUN mkdir -p /deploy/linio
RUN mkdir -p /var/log/linio/
COPY linio /deploy/linio
WORKDIR /deploy/linio
RUN python3 manage.py migrate
RUN python3 manage.py test
WORKDIR /

# Setup nginx
RUN rm /etc/nginx/sites-enabled/default
ADD docker/linio-test /etc/nginx/sites-available/linio-test
RUN ln -s /etc/nginx/sites-available/linio-test /etc/nginx/sites-enabled/linio-test
RUN echo "daemon off;" >> /etc/nginx/nginx.conf

# Setup supervisord
RUN mkdir -p /var/log/supervisor
COPY docker/supervisord.conf /etc/supervisor/conf.d/supervisord.conf
COPY docker/gunicorn.conf /etc/supervisor/conf.d/gunicorn.conf

# Share port
EXPOSE 80

# Start processes
CMD ["/usr/bin/supervisord"]
