# README #

## Small test decision ##
### Way to deploy:
1. [Install ansible](http://docs.ansible.com/ansible/intro_installation.html)
1. Install GIT
```
~$ apt-get install git
```
1. Clone repo
```
~$ git clone git@bitbucket.org:xgemio/test.git
```
1. Change dir to repo dir ansible
```
~$ cd linio_test/ansible
```
1. Create ansible/hosts: [Example](https://bitbucket.org/xgemio/test/src/5b11c0aa488ed316b732547c7626baa97a4a02d3/ansible/hosts_example?at=master&fileviewer=file-view-default)
1. Run test.yml script:
```
~/linio_test/ansible$ ansible-playbook test.yml
```