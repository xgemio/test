from django.db import models


class Visitor(models.Model):
    """ Visitor of POST /ip

    Collect ip and time of visit for every
    POST connection of /ip page

    Attributes:
        created: Time of visit.
        ip: IP-address of visitor.

    """
    created = models.DateTimeField(auto_now_add=True)
    ip = models.CharField(max_length=15, blank=False)
