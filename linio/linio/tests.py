from django.test import TestCase
from django.test.client import Client


class VisitorTestCase(TestCase):
    def setUp(self):
        self.client = Client()

    def test_404_on_wrong_path(self):
        """ Check not found pages with path errors

        """
        self.assertEqual(self.client.post('').status_code, 404)
        self.assertEqual(self.client.get('').status_code, 404)
        self.assertEqual(self.client.get('/i').status_code, 404)
        self.assertEqual(self.client.get('/ip/123').status_code, 404)
        self.assertEqual(self.client.get('/iddqd/').status_code, 404)

    def test_404_on_wrong_method(self):
        """ Check not found pages with method errors

        """
        self.assertEqual(self.client.delete('/ip/').status_code, 404)
        self.assertEqual(self.client.put('/ip/').status_code, 404)

    def test_400_on_post(self):
        """ Check wrong IP setting

        """
        response = self.client.post('/ip/', HTTP_X_FORWARDED_FOR="12")
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json()["ip"][0], "It is not valid ip.")

    def test_200_and_empty_on_get(self):
        """ Check first state

        """
        response = self.client.get('/ip/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), [])

    def test_201_on_post(self):
        """ Check setting of visitor

        """
        response = self.client.post('/ip/')
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.json()["ip"], "127.0.0.1")

    def test_200_and_non_empty_on_get(self):
        """ Check setting of visitor result

        """
        self.client.post('/ip/')
        response = self.client.get('/ip/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()[0]["ip"], "127.0.0.1")
