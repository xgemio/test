from linio.settings import logger


class TrackConnection(object):
    """ Logging user any user connection

    """
    def process_request(self, request):
        user_ip = (
            request.META.get('HTTP_X_FORWARDED_FOR', '') or
            request.META.get('REMOTE_ADDR', '')
        )
        logger.debug('{} {} {}'.format(request.method, request.path, user_ip))
