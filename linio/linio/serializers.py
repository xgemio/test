from rest_framework import serializers
from linio.models import Visitor
import datetime
import socket


def is_valid_ipv4_address(address):
    """ Check is string valid ipv4 address

    :param address: String
    :return: is_valid - Boolean
    """
    try:
        socket.inet_pton(socket.AF_INET, address)
    except AttributeError:  # no inet_pton here, sorry
        try:
            socket.inet_aton(address)
        except socket.error:
            return False
        return address.count('.') == 3
    except socket.error:  # not a valid address
        return False

    return True


def is_valid_ipv6_address(address):
    """ Check is string valid ipv6 address

    :param address: String
    :return: is_valid - Boolean
    """
    try:
        socket.inet_pton(socket.AF_INET6, address)
    except socket.error:  # not a valid address
        return False
    return True


class VisitorSerializer(serializers.Serializer):
    ip = serializers.CharField(required=True, allow_blank=False, max_length=15)
    created = serializers.DateTimeField()

    def create(self, request):
        """ Create and return a new Vizitor.

        """
        user_ip = (
            request.META.get('HTTP_X_FORWARDED_FOR', '') or
            request.META.get('REMOTE_ADDR', '')
        )
        data = {
            "ip": user_ip,
            "created": datetime.now()
        }
        return Visitor.objects.create(**data)

    def validate_ip(self, value):
        """ Validate ip-address

        """
        res = is_valid_ipv4_address(value) or is_valid_ipv6_address(value)
        if res:
            return value
        else:
            raise serializers.ValidationError("It is not valid ip.")
