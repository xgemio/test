from django.conf.urls import url
from linio import views

urlpatterns = [
    url(r'^ip/?$', views.VisitorList.as_view()),
]
