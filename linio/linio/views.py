from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
from linio.models import Visitor
from linio.serializers import VisitorSerializer
from rest_framework.views import APIView
from datetime import datetime
from linio.settings import logger


class JSONResponse(HttpResponse):
    """ An HttpResponse that renders its content into JSON.

    """
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)


class VisitorList(APIView):
    """Get all connections or set a new one.

    """
    def get(self, request, format=None):
        visitors = Visitor.objects.all()
        serializer = VisitorSerializer(visitors, many=True)
        return JSONResponse(serializer.data)

    def post(self, request, format=None):
        serializer = VisitorSerializer(request=request)
        if serializer.is_valid():
            serializer.save()
            return JSONResponse(serializer.data, status=201)
        else:
            logger.error('{} {} {}'.format(str(serializer.validated_data), serializer.errors, 400))
            return JSONResponse(serializer.errors, status=400)
